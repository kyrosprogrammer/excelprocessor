package org.bitbucket.kyrosprogrammer.excelprocessor.reflect.type;

public enum PictureSourceType {
	FILE_PATH,
	BYTE_ARRAY
}
