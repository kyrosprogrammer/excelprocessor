package org.bitbucket.kyrosprogrammer.excelprocessor.reflect.validator;

import org.bitbucket.kyrosprogrammer.excelprocessor.reflect.context.ValidatorContext;

public interface IExcelValidator {
	
	public String validate(ValidatorContext validatorContext);
	
}
