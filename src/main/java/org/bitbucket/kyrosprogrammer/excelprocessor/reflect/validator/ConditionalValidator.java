package org.bitbucket.kyrosprogrammer.excelprocessor.reflect.validator;

import org.bitbucket.kyrosprogrammer.excelprocessor.reflect.annotation.ExcelHeaderValidator;
import org.bitbucket.kyrosprogrammer.excelprocessor.reflect.annotation.ExcelSheet;
import org.bitbucket.kyrosprogrammer.excelprocessor.reflect.constant.ExcelValidatorConstant;
import org.bitbucket.kyrosprogrammer.excelprocessor.reflect.context.ValidatorContext;
import org.bitbucket.kyrosprogrammer.excelprocessor.reflect.service.AbstractCustomValidatorTask;

public class ConditionalValidator extends BaseExcelValidator {
	@Override
	public String validate(ValidatorContext validatorContext) {
		String errorMessage = null;			
		String headerKey = validatorContext.getHeaderKey(); 
		if(isUnknownExcelHeader(validatorContext, headerKey)) {
			return errorMessage;
		}
		AbstractCustomValidatorTask validatorTask = (AbstractCustomValidatorTask) validatorContext.getValidatorTask();
		if(validatorTask==null) {
			return null;
		}
		String methodName = null;
		ExcelHeaderValidator excelHeaderValidator = null;
		ExcelSheet sheetValidator = null;
		excelHeaderValidator = validatorContext.getExcelHeaderValidator();
		methodName = excelHeaderValidator.condition();
		if(!ExcelValidatorConstant.EMPTY_STRING.equals(methodName)) {
			errorMessage = CustomMethodValidator.invokeCustomTask(validatorContext,methodName,sheetValidator,excelHeaderValidator,validatorTask);
		}
		return errorMessage;
	}

	
	
	
	
}
