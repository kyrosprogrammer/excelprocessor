package org.bitbucket.kyrosprogrammer.excelprocessor.reflect.type;

public enum PictureType {
	PNG,
	JPEG,
	JPG
}
